﻿#include <iostream>
#include <algorithm>
#include <string>
#include <unordered_map>

// for little endian
void int2bin(const int &a)
{
    for (int i = sizeof(a) * 8 - 1; i >= 0 ; --i)
    {
        printf("%d", (a >> i) & 1);
    }
    printf("\n");
}

void TestInt2bin()
{
    printf(" --- Starting TestInt2bin test ---\n");
    for (int i = 0; i < 14; ++i)
    {
        int2bin(i);
    }    
    printf(" --- TestInt2bin test finished ---\n\n");
}


void RemoveDups(char* str)
{
    char* cur, * next;
    cur = next = str;
    printf("[%s] -> ", str);
    while (cur && next && *next != '\0')
    {
        ++next;
        if (*cur != *next) *++cur = *next;
    }
    printf("[%s]\n", str);  
}

void TestRemoveDups()
{
    printf(" --- Starting TestRemoveDups test ---\n");
    char data[][128] = { "AAA BBB AAA",
                         "",
                         "AAAA",
                         "AABB" };
    for (int i = 0; i < 4; ++i)
    {
        RemoveDups(data[i]);
    }
    printf(" --- TestRemoveDups test finished ---\n\n");
}


struct ListNode {
    ListNode* prev;
    ListNode* next;
    ListNode* rand; // указатель на произвольный элемент данного списка, либо NULL
    std::string     data;
    ListNode() { prev = next = rand = nullptr; }
};

class List {
public:
    List()
    {
        count = 0;
        head = tail = nullptr;
    }

    List(int count)
    {
        for (auto i = 0; i < count; ++i)
            PushFront(new ListNode);
    }

    ~List() {
        ListNode* node = head;
        while (node)
        {
            if (node->prev) Delete(node->prev);
            node = node->next;
        }
        Delete(head);
    }

    void Insert(ListNode* prev, ListNode* next, ListNode* node);
    void Delete(ListNode* node);
    void PushFront(ListNode* node)
    {
        Insert(tail, nullptr, node);
    }
    void PushBack(ListNode* node)
    {
        Insert(nullptr, head, node);
    }

    void RandomizeData();
    void RandomizeRandPointer();

    void PrintList();
    void Serialize(FILE* file);  // сохранение в файл (файл открыт с помощью fopen(path, "wb"))
    void Deserialize(FILE* file);  // загрузка из файла (файл открыт с помощью fopen(path, "rb"))

    static void TestDeserialization(int count);

private:
    ListNode* head;
    ListNode* tail;
    int       count;
};
void List::Insert(ListNode* prev, ListNode* next, ListNode* node)
{
    node->next = prev ? prev->next : head;
    if (prev) prev->next = node;

    node->prev = next ? next->prev : tail;
    if (next) next->prev = node;

    if (prev == tail) tail = node;
    if (next == head) head = node;

    count++;
}

void List::Delete(ListNode* node)
{
    if (!node) return;
    if (node->prev) node->prev->next = node->next;
    if (node->next) node->next->prev = node->prev;
    if (node == tail) tail = node->prev;
    if (node == head) head = node->next;
    count--;
    delete node;
}

void List::RandomizeData()
{
    ListNode* node = this->head;
    for (auto i = 0; node && i < count; ++i, node = node->next)
    {
        size_t length = rand() % 10 + 5;
        node->data.resize(length);
        std::generate_n(node->data.begin(), length, []() -> char { return (char)(rand() % 78 + 46); });
    }
}

void List::RandomizeRandPointer()
{
    ListNode* node = this->head;
    for (auto i = 0; node && i < count; ++i)
    {
        size_t rand_index = rand() % count;
        ListNode* rand_node = this->head;
        for (auto j = 0; rand_node && j < rand_index; ++j, rand_node = rand_node->next)
        {
            continue;
        };

        if (node)
        {
            node->rand = rand_node;
            node = node->next;
        }
    }
}

void List::PrintList()
{
    ListNode* node = head;
    if (!node)
    {
        printf("List is empty!\n");
        printf("\n");
        return;
    }

    for (auto i = 0; node && i < count; ++i, node = node->next)
    {
        printf("%p,%p,%p,%p,%s\n", node, node->next, node->prev, node->rand, node->data.c_str());
    }
    printf("\n");
}

void List::Serialize(FILE* file)
{
    fprintf_s(file, "%d\n", count);
    ListNode* node = head;
    while (node)
    {
        fprintf(file, "0x%p,0x%p,%s\n", node, node->rand, node->data.c_str());
        node = node->next;
        if(node && node->prev) Delete(node->prev);
    }
    Delete(head);
}

void List::Deserialize(FILE* file)
{
    int nodes_count = 0;
    fscanf_s(file, "%d\n", &nodes_count);

    typedef struct ListNodePair
    {
        ListNode *node;
        int id_rand;
        ListNodePair(ListNode* node, int id_rand) { this->node = node; this->id_rand = id_rand; }
    };
    std::unordered_map<int, ListNodePair> nodes_map;
    
    for (auto i = 0; i < nodes_count; ++i)
    {
        int node_id = 0;
        ListNodePair node_pair(new ListNode, 0);
        
        fscanf_s(file, "0x%p,0x%p,", &node_id, &node_pair.id_rand);

        for (char s = fgetc(file); !feof(file) && s != '\n'; s = fgetc(file)) {
            node_pair.node->data += s;
        }
        PushFront(node_pair.node);

        nodes_map.insert(std::make_pair(node_id, node_pair));
    }

    for (auto i : nodes_map)
    {
        int current_id = i.first;
        ListNodePair& curr_node_pair = i.second;
        int id_rand = curr_node_pair.id_rand;

        ListNode* rand_node = id_rand ? nodes_map.at(id_rand).node : nullptr;
        ListNode* cur_node = curr_node_pair.node;
        if (cur_node) cur_node->rand = rand_node;

    }
}

void List::TestDeserialization(int count)
{
    printf(" --- Starting deserialization test for size %d ---\n", count);

    List list(count);
    printf("Creating a new list\n");
    list.PrintList();

    list.RandomizeData();
    list.RandomizeRandPointer();
    printf("The list data and rand pointer has been randomized\n");
    list.PrintList();

    FILE* fp = nullptr;
    const char file_name[] = "test.txt";
    if (fopen_s(&fp, file_name, "wb") == 0)
    {
        list.Serialize(fp);
    }
    if (fp) fclose(fp);
    printf("The list has been serialized, please open %s to verify that \n", file_name);
    list.PrintList();

    if (fopen_s(&fp, file_name, "rb") == 0)
    {
        list.Deserialize(fp);
    }
    if (fp) fclose(fp);

    printf("The list has been deserialized\n");
    list.PrintList();

    printf(" --- Deserialization test finished ---\n\n");
}


int main()
{
    TestInt2bin();

    TestRemoveDups();

    List::TestDeserialization(0);
    List::TestDeserialization(1);
    List::TestDeserialization(2);
    List::TestDeserialization(7);
    
    return 0;
}
